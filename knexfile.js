require('dotenv').load();

module.exports = {
    client: 'pg',
    connection: "postgres://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+":"+process.env.DB_PORT+"/"+process.env.DB_DB+"",
    searchPath: [process.env.DB_DB, process.env.DB_SCHEMA],
    pool: {
        min: 0,
        max: 10
    },
    migrations: {
        directory: __dirname + '/db/migrations',
    },
    seeds: {
        directory: __dirname + '/db/seeds',
    }
  };