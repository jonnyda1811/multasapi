
exports.up = function(knex, Promise) {
    return knex.schema.withSchema('elecciones').createTable('partidos', function (table) {
        table.uuid('id').defaultTo(knex.raw('public.uuid_generate_v4()')).primary().notNullable();
        table.string('nome');
        table.integer('numero');
        table.string('logo');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('partidos');
};
