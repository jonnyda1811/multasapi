
exports.up = function(knex, Promise) {
    return knex.schema.withSchema('elecciones').createTable('candidatos', function (table) {
        table.uuid('id').defaultTo(knex.raw('public.uuid_generate_v4()')).primary().notNullable();
        table.enu('tipo',['presidente', 'senador', 'deputado', 'parlasur', 'governador', 'junta_departamental']);
        table.string('nome');
        table.string('apelido');
        table.string('foto');
        table.uuid('id_partido');
        table.integer('numero');
        table.string('nome_vice_suplente');
        table.string('apelido_vice_suplente');
        table.string('foto_vice_suplente');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('candidatos');
};
