
exports.up = function(knex, Promise) {
    return knex.schema.withSchema('elecciones').createTable('votos', function (table) {
        table.uuid('id').defaultTo(knex.raw('public.uuid_generate_v4()')).primary().notNullable();
        table.uuid('id_candidato');
        table.enu('tipo',['presidente', 'senador', 'deputado', 'parlasur', 'governador', 'junta_departamental']);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('votos');
};
