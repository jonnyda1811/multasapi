var express = require('express');
var router = express.Router();
var db = require('../modules/db');

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function busca(tipo) {
	var query = db.pg.distinct('candidatos.apelido', 'candidatos.tipo')
						.count('candidatos.apelido as total')
						.table('candidatos')
						.innerJoin('votos', 'votos.id_candidato', 'candidatos.id')
						.where('votos.tipo', tipo)
						.groupBy('candidatos.apelido', 'candidatos.tipo')
						.orderBy('total', 'DESC');

        return query.then(function (result) {
			var apelido = "1";
			result.forEach(element => {
				if (element.apelido != 'Blanco' && element.apelido != 'Nulo') {
					element.apelido = capitalizeFirstLetter(tipo) + ' ' + apelido;
					apelido++;
				}
			 });

			 return result;
		}).catch(function (e) {
            console.log(e);
        });
}

//select count(tipo) from elecciones.votos where tipo = 'presidente' group by tipo
function buscaGeral() {
	var query = db.pg.count('votos.tipo as total')
						.table('votos')
						.where('votos.tipo', 'presidente') // <<< AQUI DEIXEI ASSIM POIS SÓ PRECISAMOS DE UM TIPO
						.groupBy('votos.tipo');

        return query.then(function (result) {
			 return result;
		}).catch(function (e) {
            console.log(e);
        });
}

function buscaVotosXCadastrados() {
	var query = db.pg.count('votos.tipo as total')
						.table('votos')
						.where('votos.tipo', 'presidente') // <<< AQUI DEIXEI ASSIM POIS SÓ PRECISAMOS DE UM TIPO
						.groupBy('votos.tipo');

        return query.then(function (result) {
			return result;
		}).catch(function (e) {
            console.log(e);
        });
}

//GET APURAÇÃO
/*select count(v.tipo), nome, c.tipo from elecciones.votos v 
join elecciones.candidatos c on c.id = v.id_candidato
group by v.tipo, nome, c.tipo
order by count(v.tipo) DESC*/

//GET BRANCOS X NULOS X VÁLIDOS
/*
(select count(v.tipo), nome, c.tipo from elecciones.votos v 
join elecciones.candidatos c on c.id = v.id_candidato
where c.nome = 'Blanco' 
group by v.tipo, nome, c.tipo
order by count(v.tipo) DESC)

UNION

(select count(v.tipo), nome, c.tipo from elecciones.votos v 
join elecciones.candidatos c on c.id = v.id_candidato
where c.nome = 'Nulo'
group by v.tipo, nome, c.tipo
order by count(v.tipo) DESC)

UNION

(select count(v.tipo), 'Válidos', c.tipo from elecciones.votos v 
join elecciones.candidatos c on c.id = v.id_candidato
where c.nome != 'Blanco' and c.nome != 'Nulo'
group by v.tipo, c.tipo
order by count(v.tipo) DESC)
*/

function buscaVotosValidosXBrancosXNulos(tipo) {
	var query = db.pg.select(db.pg.raw(`count(votos.tipo), 'Brancos' as nome, candidatos.tipo`))
						.table('votos')
						.join('candidatos', 'candidatos.id', 'votos.id_candidato')
						.where('votos.tipo', tipo)
						.where('candidatos.nome', 'Blanco')
						.groupBy('candidatos.tipo')
						.union(function() {
							this.select(db.pg.raw(`count(votos.tipo), 'Nulos' as nome, candidatos.tipo`))
								.table('votos')
								.join('candidatos', 'candidatos.id', 'votos.id_candidato')
								.where('votos.tipo', tipo)
								.where('candidatos.nome', 'Nulo')
								.groupBy('candidatos.tipo')
								.union(function() {
									this.select(db.pg.raw(`count(votos.tipo), 'Válidos' as nome, candidatos.tipo`))
									.table('votos')
									.join('candidatos', 'candidatos.id', 'votos.id_candidato')
									.where('votos.tipo', tipo)
									.where('candidatos.nome', '<>', 'Nulo')
									.where('candidatos.nome', '<>', 'Blanco')
									.groupBy('candidatos.tipo')
								})
								
						}).catch(function (e) {
							console.log(e);
						});
							

        return query.then(function (result) {
			return result;
		}).catch(function (e) {
            console.log(e);
        });
}

function buscaVotosValidosXBrancosXNulosTotal() {
	var query = db.pg.select(db.pg.raw(`count(votos.*), 'Brancos' as nome`))
						.table('votos')
						.join('candidatos', 'candidatos.id', 'votos.id_candidato')
						.where('candidatos.nome', 'Blanco')
						.where('candidatos.tipo', 'presidente')
						.union(function() {
							this.select(db.pg.raw(`count(votos.*), 'Nulos' as nome`))
								.table('votos')
								.join('candidatos', 'candidatos.id', 'votos.id_candidato')
								.where('candidatos.nome', 'Nulo')
								.where('candidatos.tipo', 'presidente')
								.union(function() {
									this.select(db.pg.raw(`count(votos.*), 'Válidos' as nome`))
									.table('votos')
									.join('candidatos', 'candidatos.id', 'votos.id_candidato')
									.where('candidatos.nome', '<>', 'Nulo')
									.where('candidatos.nome', '<>', 'Blanco')
									.where('candidatos.tipo', 'presidente')
								})
								
						}).catch(function (e) {
							console.log(e);
						});
							

        return query.then(function (result) {
			return result;
		}).catch(function (e) {
            console.log(e);
        });
}

//GET DASHBOARD
router.get('/dashboard', async function(req, res, next) {
    if (req.header('key') !== 'e9c4AtCp5khzw5Nt') {
        res.status(401).json({
            status: "error",
            message: "Forbidden"
        });
    } else {
		var presidentes = await busca('presidente');
		var senadores = await busca('senador');
		var parlasur = await busca('parlasur');
		var deputados = await busca('deputado');
		var governadores = await busca('governador');
		var junta_departamental = await busca('junta_departamental');
		var geral = await buscaGeral();
		var presidentesConsolidado = await buscaVotosValidosXBrancosXNulos('presidente');
		var senadoresConsolidado = await buscaVotosValidosXBrancosXNulos('senador');
		var parlasurConsolidado = await buscaVotosValidosXBrancosXNulos('parlasur');
		var deputadosConsolidado = await buscaVotosValidosXBrancosXNulos('deputado');
		var governadoresConsolidado = await buscaVotosValidosXBrancosXNulos('governador');
		var juntaDepartamentalConsolidado = await buscaVotosValidosXBrancosXNulos('junta_departamental');
		var votosConsolidado = await buscaVotosValidosXBrancosXNulosTotal();

		//Insere o número fictício de pessoas vontantes
		geral[0].votantes = 4135679;
		
		res.status(200).json({
			status: "success",
			data: [{'geral': geral}, {'votosConsolidado': votosConsolidado}, {'presidentesConsolidado': presidentesConsolidado}, {'senadoresConsolidado': senadoresConsolidado}, {'parlasurConsolidado': parlasurConsolidado}, {'deputadosConsolidado': deputadosConsolidado}, {'governadoresConsolidado':governadoresConsolidado}, {'juntaDepartamentalConsolidado': juntaDepartamentalConsolidado}, {'presidentes': presidentes}, {'senadores': senadores}, {'parlasur': parlasur}, {'deputados': deputados}, {'governadores': governadores}, {'junta_departamental': junta_departamental}],
			message: "Record(s) retrieved with success."
		});
    }
});

function buscaCandidato(numero_candidato) {
	var query = db.pg.select('candidatos.*', 'partidos.logo', 'partidos.nome as nome_partido')
						.table('candidatos')
						.innerJoin('partidos', 'partidos.id', 'candidatos.id_partido')
						.where('candidatos.numero', numero_candidato);

        return query.then(function (result) {
			 return result;
		}).catch(function (e) {
            console.log(e);
        });
}

//GET CANDIDATO
router.get('/candidato', async function(req, res, next){
	if (req.header('key') !== 'e9c4AtCp5khzw5Nt') {
        res.status(401).json({
            status: "error",
            message: "Forbidden"
        });
    } else {
		var numero_candidato = req.body.numero_candidato;
		var candidato = await buscaCandidato(numero_candidato);
		
		res.status(200).json({
			status: "success",
			data: candidato,
			message: "Record(s) retrieved with success."
		});
	}
});

//POST VOTO
router.post('/voto', async function(req, res, next) {
    if (req.header('key') !== 'e9c4AtCp5khzw5Nt') {
        res.status(401).json({
            status: "error",
            message: "Forbidden"
        });
    } else {
		var tipo = req.body.tipo;
		var numero_candidato = req.body.numero_candidato;
		var hash = req.body.hash;

		var candidato = await buscaCandidato(numero_candidato);

		db.pg.insert({id_candidato: candidato[0].id, hash: hash, tipo: tipo})
			.table('votos')
			.then(function(result){
				res.status(200).json({
					status: "success",
					data: "Record(s) inserted with success.",
					message: "Record(s) inserted with success."
				});
			});
	}
});

function buscaPessoa(nome, identidade) {
	var query = db.pg_bio.select('*').table('people');
	
	if (nome != '') {
		query.where('full_name', 'LIKE', nome);
	}
	if (identidade != '') {
		query.where('cpf', identidade);
	}

	return query.then(function (result) {
			return result;
	}).catch(function (e) {
		console.log(e);
	});
}

//GET PESSOA
router.get('/pessoa', async function(req, res, next){
	if (req.header('key') !== 'e9c4AtCp5khzw5Nt') {
        res.status(401).json({
            status: "error",
            message: "Forbidden"
        });
    } else {
		var nome = req.body.nome;
		var identidade = req.body.identidade;
		var pessoa = await buscaPessoa(nome,identidade);
		
		res.status(200).json({
			status: "success",
			data: pessoa,
			message: "Record(s) retrieved with success."
		});
	}
});

module.exports = router;
