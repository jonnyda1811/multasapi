
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('partidos').del()
    .then(function () {
      // Inserts seed entries
      return knex('partidos').insert([
        {nome: 'Partido Blanco', numero: '1', logo: 'images/partidos/partido_blanco.png'},
        {nome: 'Partido Azul', numero: '2', logo: 'images/partidos/partido_azul.png'},
        {nome: 'Partido Rojo', numero: '3', logo: 'images/partidos/partido_rojo.png'},
        {nome: 'Partido Verde', numero: '4', logo: 'images/partidos/partido_verde.png'}
      ]);
    });
};
