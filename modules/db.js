var pg = require("knex")({
    client: "pg",
    connection: "postgres://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+":"+process.env.DB_PORT+"/"+process.env.DB_DB+"",
    searchPath: [process.env.DB_DB, process.env.DB_SCHEMA],
    pool: {
        min: 0,
        max: 10
    }
  });

var pg_bio = require("knex")({
    client: "pg",
    connection: "postgres://"+process.env.DB_BIO_USER+":"+process.env.DB_BIO_PASS+"@"+process.env.DB_BIO_HOST+":"+process.env.DB_BIO_PORT+"/"+process.env.DB_BIO_DB+"",
    searchPath: [process.env.DB_BIO_DB, process.env.DB_BIO_SCHEMA],
    pool: {
        min: 0,
        max: 10
    }
  });

module.exports = {
  pg, pg_bio
};